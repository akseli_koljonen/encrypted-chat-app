# coding=utf-8
import ConfigParser
import base64
import sys, socket, select
import hashlib, binascii
import os
import signal
from cryptograph import AESCipher

os.system("clear")
#signal handler
def sigint_handler(signum, frame):
    print('\nUser interrupt')
    sys.exit()

signal.signal(signal.SIGINT, sigint_handler)

config = ConfigParser.RawConfigParser()
config.read(r'config.conf')
HOST = config.get('config', 'HOST')
PORT = int(config.get('config', 'PORT'))
PASSWORD = config.get('config', 'PASSWORD')
PASSWORD.encode('utf-8')
key = hashlib.pbkdf2_hmac('sha256', PASSWORD, b'salt', 100000) # Hash and salt the password
BLOCKLIST = config.get('config', 'BLOCKLIST') # Block list by IP-address
SOCKET_LIST = []
RECV_BUFFER = 5096


def chat_server():
    aes = AESCipher()
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(10)
    SOCKET_LIST.append(server_socket)
    print("Server started on port " + str(PORT))

    while 1:
        ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST,[],[],0)
        for sock in ready_to_read:
            if sock == server_socket:
                sockfd, addr = server_socket.accept()
                if addr[0] in BLOCKLIST: # Check if ip-address is in the block list
                    sockfd.close()
                    print("New user IP-address is in blocklist")
                else:
                    SOCKET_LIST.append(sockfd) # Append new socket to socket list
                    print "user (%s, %s) connected" % addr
                    broadcast(server_socket, sockfd, aes.encrypt(key,"[%s:%s] entered chat\n" % addr))
            else:
                try:
                    data = sock.recv(RECV_BUFFER) # Read data from buffer
                    if data:
                        broadcast(server_socket, sock,data)
                    else:
                        # Remove socket
                        if sock in SOCKET_LIST:
                            SOCKET_LIST.remove(sock)
                        broadcast(server_socket, sock,aes.encrypt(key,"user (%s, %s) is offline\n" % addr))
                except:
                    broadcast(server_socket, sock, "user (%s, %s) is offline\n" % addr)
                    continue

    server_socket.close()

# broadcast chat messages to all connected clients
def broadcast (server_socket, sock, message):
    for socket in SOCKET_LIST:
        # send the message
        if socket != server_socket and socket != sock:
            try:
                socket.send(message)
            except:
                # Broken socket connection
                socket.close()
                # Remove broken socket
                if socket in SOCKET_LIST:
                    SOCKET_LIST.remove(socket)

if __name__ == "__main__":
    chat = chat_server()
    run.chat
    sys.exit()
