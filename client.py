# coding=utf-8
import sys, socket, select
import base64
from Crypto.Cipher import AES
import hashlib, binascii
import signal
import hmac
import os
from cryptograph import AESCipher

os.system("clear")
print("""\n──▄▀▀▀▄───────────────
──█───█───────────────
─███████─────────▄▀▀▄─
░██─▀─██░░█▀█▀▀▀▀█░░█░
░███▄███░░▀░▀░░░░░▀▀░░

WELCOME TO ENCRYPTED CHAT!
""")

#signal handler
def sigint_handler(signum, frame):
	print('\nUser interrupt!')
	sys.exit()

signal.signal(signal.SIGINT, sigint_handler)

def chat_client():
	if(len(sys.argv) < 5) :
		print('Wrong input, try: python client.py <host ip> <port> <password> <nick_name>')
		sys.exit()

	#initialize variables
	host = sys.argv[1]
	port = int(sys.argv[2])
	password = sys.argv[3]
	password.encode('utf-8')
	name = sys.argv[4]
	aes = AESCipher()
	key = hashlib.pbkdf2_hmac('sha256', password, b'salt', 100000) # Hash and salt the password
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # initialize socket
	s.settimeout(3)

	try:
		s.connect((host, port))

	except:
		print("Unable to connect \n")
		sys.exit()

	print("Welcome to the chat!\n")

	while 1:
		socket_list = [sys.stdin, s]
		read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])

		for sock in read_sockets:
			if sock == s:

				data = sock.recv(5096)

				if not data:
					print("Disconnected from chat server\n")
					sys.exit()
				else:
					if data.find("|") != -1:
						data, a_mac = data.split("|") # Split message and mac
						message = aes.decrypt(key,data)
						mac = hmac.new(key, data, hashlib.sha1).hexdigest()
						if hmac.compare_digest(mac, a_mac) == False: # Compare macs
							print("This message may have been edited! \n")

					else:
						message = aes.decrypt(key,data)
					sys.stdout.write(message)

			else:
				msg = sys.stdin.readline()
				msg = name + ": "+ msg # Add nickname to message
				msg = aes.encrypt(key,msg)
				mac = hmac.new(key, msg, hashlib.sha1).hexdigest() # Create mac
				msg = msg + "|" + mac # Add mac to message
				s.send(msg)

if __name__ == "__main__":
	client = chat_client()
	run.client
	sys.exit()
