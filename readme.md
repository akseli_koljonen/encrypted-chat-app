Encrypted chat is a simple chat application script written in python 2.7.x.
It uses AES mode CBC encryption to transfer messages over the public network
securely.  

It consists of three scripts: a server, a client and a cryptograph.  
Run the server.py in a system so that the client client.py can connect from
remote system with the correct password.
It is possible to connect multiple clients to a single server to create
a group chat.
Edit password, host and port in config.conf file

SERVER.PY USAGE
$ python server.py

CLIENT.PY USAGE
$ python client.py "host" "port" "password" "username"
