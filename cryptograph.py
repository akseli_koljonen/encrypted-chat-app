import base64
from Crypto.Cipher import AES
from Crypto import Random

class AESCipher():

    def encrypt(self, key, raw):
        raw = self._pad(raw) # Plain text is padded to bring it to the length of the block size
        iv = Random.new().read(AES.block_size) # Initialization vector
        cipher = AES.new(key, AES.MODE_CBC, iv) # Create the AES cipher
        return base64.b64encode(iv + cipher.encrypt(raw)) # Return enrypted and ecoded text

    def decrypt(self, key, enc):
        enc = base64.b64decode(enc) # Decode encrypted text
        iv = enc[:AES.block_size] # Initialization vector
        cipher = AES.new(key, AES.MODE_CBC, iv) # Create the AES cipher
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8') # Decrypt and unpad cipher text

    def _pad(self, s):
        length = AES.block_size - (len(s) % AES.block_size)
        s += chr(length)*length
        return s

    @staticmethod
    def _unpad(s):
        return s[0:-ord(s[-1])]
